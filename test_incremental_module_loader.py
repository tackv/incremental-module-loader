import pytest
import incremental_module_loader as iml

@pytest.fixture()
def module_loader():
    return iml.IncrementalModuleLoader()

def a_module():
    pass
    
def a_module_with_optional_param(foo=None):
    return f'foo is {foo}'

class ObjectModule():
    def __init__(self):
        pass
    
class ObjectModuleThatDependsOnFoo():
    def __init__(self, foo):
        self.foo = foo
        self.bar = None
        
    def function_module_that_depends_on_bar(self, bar):
        self.bar = bar

def test_load_object(module_loader):
    
    foo = module_loader.load(foo=ObjectModule)
    bar = module_loader.load(bar=ObjectModuleThatDependsOnFoo)
    assert bar.foo == foo
    
    # This also validates the dynamic injection: the function only takes bar as parameter, 
    # but we have the module foo also.
    _ = module_loader.load(_=bar.function_module_that_depends_on_bar)
    assert bar.bar == bar
    
    assert len(module_loader.modules) == 3
    
def test_anonymous(module_loader):
    foo = module_loader.load(foo=ObjectModule)
    
    bar = module_loader.load(ObjectModuleThatDependsOnFoo)
    
    assert len(module_loader.modules) == 1, "Anonymous modules should not be present in the list of modules"
    
def test_module_update(module_loader):
    module_loader.update(
        foo='hello'
    )
    bar = module_loader.load(ObjectModuleThatDependsOnFoo)
    
    assert bar.foo == 'hello'
    
def test_module_refuses_anonymous_with_named(module_loader):
    with pytest.raises(iml.IncrementalModuleError):
        module_loader.load(a_module, named=a_module)    
        
def test_module_only_authorizes_a_single_named(module_loader):
    with pytest.raises(iml.IncrementalModuleError):
        module_loader.load(named1=a_module, named2=a_module)   
        
def test_module_refuses_duplicate_modules(module_loader):
    foo = module_loader.load(foo=ObjectModule)
    
    with pytest.raises(iml.IncrementalModuleError):
        # foo= is already declared
        module_loader.load(foo=ObjectModule)
        
def test_module_not_present(module_loader):
    with pytest.raises(iml.IncrementalModuleError):
        bar = module_loader.load(ObjectModuleThatDependsOnFoo)
    
def test_getitem(module_loader):
    obj = module_loader.load(name=ObjectModule)
    assert module_loader['name'] == obj
    
def test_module_with_optional_param(module_loader):
    foo = module_loader.load(foo=a_module)
    obj = module_loader.load(a_module_with_optional_param)

def test_module_with_optional_param_no_module(module_loader):
    module_loader.load(a_module_with_optional_param)
    assert module_loader.get('foo') == None

def test_module_with_kwargs(module_loader):
    def a_module_with_optional_param(foo, **kwargs):
        # discards but requires foo, returns kwargs
        return kwargs

    foo = module_loader.load(foo=a_module)
    modules = {'x': 'x', 'y': 'y'}
    module_loader.update(modules)

    assert module_loader.load(a_module_with_optional_param) == modules

def test_module_update_multiple_times(module_loader):
    
    module_loader.update(x='x')
    module_loader.update(y='y')

    assert 'x' in module_loader
    assert 'y' in module_loader


